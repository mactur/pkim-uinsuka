
    <?php include_once('header.php') ?>
      <div id="list-post-wrap">
          <div class="container">
            <div class="col-md-9 list-into-single"> 
            <div>
                <p class="list-page-single"><a href="#">Beranda</a></p>>><p class="list-page"><a href="#">Search</a></p>
            </div>
        </div>
            <div class="col-md-9 single-post-posts">
               
                <div id="title-list-posts-wrap">
                    <h2 class="title-section" style="text-align:left">Hasil Pencarian : " <?php @$id = $_GET['cari'];
             
             if (!empty($id)) {
               echo $id;
            }else {
              echo "Cari disini";
            }
              ?> "</h2>
                    <div class="underscore" style="margin-left:0px;margin-right:0px;"></div>
                </div>
                <?php 

        if(count($cari)>0){
                foreach ($cari as $data) {
                    ?>
                <div class="panel-post-wrap">
                    <div class="col-sm-4 img-list-posts-wrap">
                        <img src="<?= base_url() ?>assets/img/<?= $data['gambar'] ? $data['gambar'] : 'noimage.png' ?>" class="img-responsive img-list-posts"/>
                    </div>
                    <div class="col-sm-8">
                        <h3 class="title-isi-list-posts"><a href="<?php echo base_url() ?>single/<?php echo $data['slug'] ?>"><?php echo $data['judul']; ?></a></h3>
                        <div class="detail-post detail-post-list-posts">
                            <p class="date-post">
                                <span class="glyphicon glyphicon-dashboard" style="margin-right:5px;color:#29CC6D"></span><b>Tanggal :</b> 
                                <span class="text-date-post"><?= date('d M Y', strtotime($data['created_at'])) ?></span>
                            </p>
                            <p class="created-post">
                                <span class="glyphicon glyphicon-user"  style="margin-right:5px;color:#29CC6D"></span><b>Ditulis oleh : </b>
                                <span class="text-created-post"><?= $data['penulis'] ?></span>
                            </p>
                        </div>
                        <div class="isi-lists-posts">
                            <p>
                                <?php echo mb_substr($data['content'], 0, 200)."..." ?>
                            </p>
                        </div>
                        <a href="<?php echo base_url() ?>single/<?php echo $data['slug'] ?>">
                        <button type="button" class="btn btn-success">Read More</button>
                    </a>
                    </div>
                </div>
            <?php }}else {
                echo "Data tidak Ditemukan";
            }

             ?>
               
                <div class="col-sm-12 pagination-wrap">
                 
                </div>
            </div>
            <div id="wrap-sidebar-single" class="col-md-3">
        <?php include("sidebar.php") ?>
    </div>
          </div>
      </div>
      <?php include_once('footer.php') ?>
