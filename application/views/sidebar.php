<div id="populer-wrap" class="col-md-12 col-sm-6">
    <div class="title-sidebar">
        <h4>
            <span class="glyphicon glyphicon-file"></span>
            <b >POST</b> POPULER
        </h4>
        <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
    </div>

    <div id="post-populer-sidebar">
        <ul id="post-populer-sidebar-list">
            <?php foreach ($populer as $pop): ?>
              <li><a href="<?= base_url().'single/'.$pop['slug'] ?>"><span class="glyphicon glyphicon-file" style="margin-right:5px"></span><?= $pop['judul'] ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div id="agenda-wrap" class="col-md-12 col-sm-6">
    <div class="title-sidebar" style="margin-top:15px;">
        <h4>
            <span class="glyphicon glyphicon-calendar"></span>
            <b >AGENDA</b> TERKINI

        </h4>
        <div class="underscore" style="margin-left:0px;margin-left:0px;margin-bottom:15px;"></div>
    </div>
    <div id="agenda-list-sidebar">
        <div class="col-sm-12 agenda-wrapper">
            <ul style="padding-left:5px">
              <?php foreach ($agenda as $ag): ?>
                <li class="agenda-post-sidebar">
                  <a href="<?= base_url().'agenda/'.$ag['slug'] ?>">
                    <i class="glyphicon glyphicon-calendar" style="margin-right:5px"></i>
                    <?= $ag['judul'] ?><p class="date-agenda-sidebar"><?= date('d M Y', strtotime($ag['created_at'])) ?></p>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
