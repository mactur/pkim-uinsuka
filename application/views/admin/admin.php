

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row" style="padding-left:15px;padding-right:15px">
            <div class="alert alert-success alert-dismissable" style="background-color:rgba(124, 231, 129, 0.92) !important;padding-top:10px;padding-bottom:5px;border:none">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <h4><strong>Success, </strong>Selamat Datang Admin ! </h4>
            </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
     
      <center>
      <div class="row">
        <div class="col-md-12">
        <div class="col-lg-4 col-sm-6 col-xs-12" >
             <div class="panel-group">
                    <div class="panel panel-info">
                      <div class="panel-heading">Total Blog</div>
                      <div class="panel-body"><b><?php echo $blg; ?> Blogs</b></div>
                  </div>
              </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12">
            <div class="panel-group">
                    <div class="panel panel-warning">
                      <div class="panel-heading">Jumlah Komentar</div>
                      <div class="panel-body"><b><?php echo $komen; ?>  Komentar</b></div>
                  </div>
              </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-xs-12">
             <div class="panel-group">
                    <div class="panel panel-success">
                      <div class="panel-heading">Total Like</div>
                      <div class="panel-body"><b><?php echo $suka; ?></b></div>
                  </div>
              </div>
        </div>
      </div>
    </div>

   </center>
   

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">

                 <section class="content-header">
      <h1>
        Log Aktivitas 
        <small>Data log aktivitas admin terbaru</small>
      </h1>
      
    </section>

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Melakukan</th>
                                <th>Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                                                        
                          
                            <tr class='warning'                            >
                                <td>1</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Mengedit dan Menganti Foto Sebuah Portofolio :WORKSHOP EVALUASI PENELITIAN</td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                                                        
                          
                            <tr class='danger'                            >
                                <td>2</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Menghapus Sebuah Blog :Blog</td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                                                        
                          
                            <tr class='danger'                            >
                                <td>3</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Menghapus Sebuah Blog :amls</td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                                                        
                          
                            <tr class='warning'                            >
                                <td>4</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Mengedit settingan di About </td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                                                        
                          
                            <tr class='warning'                            >
                                <td>5</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Mengedit Sebuah Project :cek</td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                                                        
                          
                            <tr class='warning'                            >
                                <td>6</td>
                                <td class="txt-oflo">Admin</td>
                                <td>Mengedit dan Menganti Foto Sebuah Portofolio :mencoba</td>
                                <td class="txt-oflo">April 18, 2017</td>
                            </tr>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  