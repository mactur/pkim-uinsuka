
<?php include_once('header.php') ?>
<div id="carousel-wrap">
  <div id="carousel-wrap-in" class="container">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner slider-post">
        <?php $i=0; foreach ($slides as $slide): ?>
          <div class="item <?php if ($i<1): ?>
            active
          <?php endif; ?>">
            <img src="<?= base_url() ?>assets/img/slides/<?= $slide['gambar'] ?>" alt="...">
            <div class="carousel-caption">
              <h2><?= $slide['status'] ?></h2>
              <p style="font-family: myf">
               <?= $slide['caption'] ?>
             </p>
           </div>
         </div>
        <?php $i++; endforeach; ?>
     </div>

 <!-- Controls -->
 <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
  <span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
  <span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>
</div>
</div>
<div id="popular-wrap">
</div>
<div id="popular-wrap-bottom">
  <h2 class="title-section">POPULAR POST</h2>
  <div class="underscore"></div>
  <div class="container">
    <?php foreach ($populer as $pop): ?>
      <div data-aos="fade-up">
       <div class="col-sm-4 post-populer">
         <img class="img-responsive img-popular" src="<?= base_url() ?>assets/img/thumb/<?= $pop['gambar'] ? $pop['gambar'] : 'noimage.png' ?>">
         <h3>
           <a href="<?= base_url().'single/'.$pop['slug'] ?>" class="title-post-popular"><?= $pop['judul'] ?></a>
         </h3>
         <h6 style="color:#555;font-family: myf">Penulis : <b><?= $pop['penulis'] ?></b><span style="margin-left:10px"> Tanggal : <b><?= date('d M Y', strtotime($pop['created_at'])) ?></b></span></h6>
         <p class="content-popular-post">
           <?= substr(strip_tags($pop['content']), 0, 200) ?>...
         </p>
         <a href="<?= base_url().'single/'.$pop['slug'] ?>" style="color:green">Selengkapnya >> </a>
       </div>
     </div>
    <?php endforeach; ?>

</div>
</div>
<div class="col-sm-12" id="activity">
  <div class="container">
    <h2 class="title-section" style="color:#fff;margin-top:60px">POST TERBAIK</h2>
    <div class="underscore"></div>


    <div class="col-md-12">
      <div id="news-slider" class="owl-carousel">
        <?php foreach ($best as $b): ?>
          <div data-aos="zoom-in-up">
            <div class="post-slide">
              <div class="post-img">
                <a href="<?= base_url().'single/'.$pop['slug'] ?>"><img src="<?= base_url() ?>assets/img/thumb/<?= $b['gambar'] ? $b['gambar'] : 'noimage.png' ?>" alt=""></a>
              </div>

              <div class="post-content">
                <div class="post-date">
                  <span class="month"><?= date('d', strtotime($b['created_at'])) ?></span>
                  <span class="date"><?= date('m', strtotime($b['created_at'])) ?></span>
                  <span class="month"><?= date('Y', strtotime($b['created_at'])) ?></span>
                </div>

                <h5 class="post-title"><a href="<?= base_url().'single/'.$b['slug'] ?>"><?= $b['judul'] ?></a></h5>
                <a href="#" class="isi-suka">
                  <p class="post-description">
                    <?= substr(strip_tags($b['content']), 0, 185) ?>...
                  </p>
                </a>
              </div>
              <ul class="post-bar">
                <li>Penulis : <a href="<?= base_url().'penulis/'.$b['nim'] ?>"> <?= $b['penulis'] ?></a> </li>
              </ul>
            </div>
          </div>
        <?php endforeach; ?>
    </div>
  </div>


</div>
</div>
<div id="all-post-wrapper">
  <div class="container-fluid">
    <div class="col-sm-3">
      <h3 class="title-section title-section-bottom" style="line-height: 1.3em">MAHASISWA <br> TERAKTIF</h3>
      <div class="underscore"></div>
      <?php foreach ($mahasiswa as $maha): ?>
        <div data-aos="zoom-in">
         <a href="<?= base_url().'penulis/'.$maha['nim'] ?>" >
          <div class="col-sm-12 post-pengumuman">
            <div class="col-sm-4 col-xs-5 img-pengumuman-wrap">
              <img class="img-responsive img-pengumuman" src="<?= base_url() ?>assets/img/trophy.svg"/>
            </div>
            <div class="col-sm-8 col-xs-7 main-pengumuman">
              <a href="<?= base_url().'penulis/'.$maha['nim'] ?>" class="aktif">
                <h5 class="title-pengumuman"><?= $maha['nim'] ?></h5>
                <h5 class="title-pengumuman" style="color:#555"><?= $maha['nama'] ?></h5>
                <h6 class="title-pengumuman" style="color:#6d9c6f"><?= $maha['total'] ?> Posts</h6>
              </a>
            </div>
          </div>
        </a>
      </div>
      <?php endforeach; ?>
  </div>
<div id="recent-post" class="col-sm-6">
  <h3 class="title-section title-section-bottom">POST TERBARU</h3>
  <div class="underscore"></div>
  <?php foreach ($new as $n): ?>
    <div data-aos="zoom-in">
      <div class="col-sm-12 post-recent">
        <div class="col-sm-4 col-xs-5 img-recent-wrap">
          <img class="img-responsive img-recent" src="<?= base_url() ?>assets/img/thumb/<?= $n['gambar'] ? $n['gambar'] : 'noimage.png' ?>"/>
        </div>
        <div class="col-sm-8 col-xs-7 main-pengumuman">
         <a href="<?= base_url().'single/'.$n['slug'] ?>">
          <h4 class="title-recent"><?= $n['judul'] ?></h4>
          </a>
          <h6 style="color:#555;font-family: myf">Penulis : <b><?= $n['penulis'] ?></b><span style="margin-left:10px"> Tanggal : <b><?= date('d M Y', strtotime($n['created_at'])) ?></b></span></h6>
          <p>
            <?= substr(strip_tags($n['content']), 0, 200) ?>...
          </p>
          <a href="<?= base_url().'single/'.$n['slug'] ?>" style="color:green">Selengkapnya >> </a>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</div>
<div class="col-sm-3">
  <h3 class="title-section title-section-bottom">AGENDA</h3>
  <div class="underscore"></div>
  <div class="col-sm-12 agenda-wrapper">
    <ul>
      <?php foreach ($agenda as $ag): ?>
        <div data-aos="zoom-in">
          <a href="<?= base_url().'single/'.$ag['slug'] ?>"><li class="agenda-post"><i class="glyphicon glyphicon-calendar" style="margin-right:5px"></i><?= $ag['judul'] ?><p class="date-agenda"><?= date('d M Y', strtotime($ag['created_at'])) ?></p></li></a>
        </div>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
</div>
</div>
<?php include_once('footer.php') ?>
