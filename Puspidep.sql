-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 07:43 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Puspidep`
--

-- --------------------------------------------------------

--
-- Table structure for table `About`
--

CREATE TABLE `About` (
  `id_about` int(11) NOT NULL,
  `isi_about` text NOT NULL,
  `place` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `About`
--

INSERT INTO `About` (`id_about`, `isi_about`, `place`) VALUES
(1, '<h2 >Pusat Pengkajian Islam,Demokrasi dan Perdamaian</h2>\r\n\r\n          <p>Pusat Pengkajian Islam Demokrasi dan Perdamaian (PusPIDeP) adalah lembaga profesional yang bergerak di bidang sosial, keagamaan, dan kemanusiaan. PusPIDeP mempunyai komitmen kuat dalam melakukan penelitian, advokasi, diseminasi dan publikasi terkait topik-topik keislaman dan demokrasi serta berpartisipasi aktif dalam mengupayakan perdamaian dan dialog antar iman. Untuk itu, PusPIDeP membuka diri untuk bekerja sama dengan pihak-pihak terkait baik dari dalam maupun luar negeri.</p>', '2'),
(2, 'ab1.jpg', '1'),
(3, 'Sejarah lahirnya puspidep yaitu: On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word \"and\" and the Little Blind Text should turn around and return to its own, safe country.', '3'),
(4, '  <p>Puspidep merupakan lembaga penelitian yang berfokus pada kajian <strong>keislaman</strong> <strong>demokrasi</strong> dan <strong>perdamaian</strong></p>\r\n                      <ul>\r\n                        <li>Puspidep tidak hanya melakukan kegiatan riset saja akan tetepi juga turut serta aktif melakukan kegiatan Seminar,workshop,FGd,dan sebagainya.</li>\r\n                      </ul>', '4'),
(5, 'Visi PusPIDeP adalah menciptakan kehidupan sosial dan masyarakat Indonesia berdasarkan pada nilai-nilai kebangsaan, keagamaan, dan kemanusiaan, serta turut berkontribusi untuk menciptakan masyarakat Muslim yang bermartabat dan berkeadaban dengan mengintegrasikan gagasan keislaman, kebangsaan dan demokrasi.', '5'),
(6, 'Untuk mewujudkan visi tersebut, PusPIDeP memandang perlu untuk mengambil langkah-langkah strategis dengan:\r\n1.Mengadakan penelitian sosial dan pemberdayaan masyarakat dalam isu kajian Islam, kajian agama.\r\n2.Melaksanakan pelatihan dan pendidikan masyarakat dalam rangka penenaman dan penguatan nilai-nilai demokrasi dan perdamaian.\r\n3.Mendorong peran pemeluk agama dan masyarakat dalam demokrasi, serta peran pemeluk agama dan masyarakat dalam menciptakan dan menjaga perdamaian dalam semua lingkup kehidupan.\r\n4.Mengadakan pelatihan dan pendampingan manajemen dan resolusi konflik di masyarakat dalam rangka penanaman dan penguatan demokrasi dan perdamaian.\r\n5.Menjembatani jarak antara kajian-kajian akademik, aspirasi masarakat dan arah kebijakan pemerintah dalam upaya ikut merumuskan rekomendasi kebijakan dalam bidang keislaman, demokrasi dan perdamaian.', '6');

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE `Admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Admin`
--

INSERT INTO `Admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '77c25370d112c28199a1be6117dcc189');

-- --------------------------------------------------------

--
-- Table structure for table `Blog`
--

CREATE TABLE `Blog` (
  `id_blog` int(5) NOT NULL,
  `judul_blog` varchar(40) NOT NULL,
  `kategori` varchar(40) NOT NULL,
  `status` int(1) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `tgl_blog` date NOT NULL,
  `gambar_blog` varchar(50) NOT NULL,
  `isi_blog` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Blog`
--

INSERT INTO `Blog` (`id_blog`, `judul_blog`, `kategori`, `status`, `deskripsi`, `tgl_blog`, `gambar_blog`, `isi_blog`) VALUES
(39, 'Blog', '', 2, 'asaa', '2018-08-06', '7e4a47fd389d6944ed54a452fe2f3bab.jpg', '<p>sas</p>');

-- --------------------------------------------------------

--
-- Table structure for table `Contact`
--

CREATE TABLE `Contact` (
  `id_contact` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `pesan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Log`
--

CREATE TABLE `Log` (
  `id_log` int(11) NOT NULL,
  `do` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL,
  `siapa` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Portofolio`
--

CREATE TABLE `Portofolio` (
  `id_portofolio` int(11) NOT NULL,
  `Judul_portofolio` varchar(40) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `status` varchar(3) NOT NULL,
  `Gambar_portofolio` varchar(100) NOT NULL,
  `isi_portofolio` text NOT NULL,
  `tgl_portofolio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Portofolio`
--

INSERT INTO `Portofolio` (`id_portofolio`, `Judul_portofolio`, `deskripsi`, `status`, `Gambar_portofolio`, `isi_portofolio`, `tgl_portofolio`) VALUES
(8, 'aasas', '																																																																						mantap																																																															', '2', '1a3d0caf7e707f102a39fe0392bc9122.jpg', '<p>sasaasasasasasasasasa</p>', '2018-08-06'),
(10, 'dsds', 'asasas', '2', '3239755c9b0562798b78f0ef55798eed.jpg', '<p>sdsdsd</p>', '2018-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `Project`
--

CREATE TABLE `Project` (
  `id_project` int(11) NOT NULL,
  `judul_project` varchar(40) NOT NULL,
  `logo_project` varchar(40) NOT NULL,
  `deskripsi_project` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project`
--

INSERT INTO `Project` (`id_project`, `judul_project`, `logo_project`, `deskripsi_project`) VALUES
(1, 'RISET PENELITIAN', 'flaticon-worker', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.'),
(2, 'WORKSHOP', 'flaticon-sketch', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.'),
(3, 'FOCUS GROUP DISCCUSION', 'flaticon-engineering', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.\r\n\r\n'),
(4, 'SEMINAR', 'flaticon-crane', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.');

-- --------------------------------------------------------

--
-- Table structure for table `Project_post`
--

CREATE TABLE `Project_post` (
  `id_project_post` int(4) NOT NULL,
  `judul_project` varchar(150) NOT NULL,
  `status` varchar(5) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `tgl_project` date NOT NULL,
  `gambar_project` varchar(200) NOT NULL,
  `isi_project` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_post`
--

INSERT INTO `Project_post` (`id_project_post`, `judul_project`, `status`, `deskripsi`, `tgl_project`, `gambar_project`, `isi_project`) VALUES
(12, 'ntapssss', '2', '																																																		Mantap																																												', '2018-08-06', '9df32619b3d65afeb84cf42178e9e9cf.jpg', '<p>sassasacededede</p>');

-- --------------------------------------------------------

--
-- Table structure for table `Slider`
--

CREATE TABLE `Slider` (
  `id_slider` int(11) NOT NULL,
  `gambar_slider` varchar(240) NOT NULL,
  `judul` varchar(240) NOT NULL,
  `link` varchar(240) NOT NULL,
  `sub_content` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Slider`
--

INSERT INTO `Slider` (`id_slider`, `gambar_slider`, `judul`, `link`, `sub_content`) VALUES
(1, 'a3cc9ddaec175d9d15b83b9bebc5e068.jpg', 'Literatur  Keislaman  Millenial                                                                                                                                                                                                                 ', 'a', 'cek                                                                                                                                                                                                                                                                                                         '),
(2, 'bg2.jpeg', 'Peta Literatur Keislaman  Pilenial: Ideologi dan Jaringan                                                                                ', 's', 'Penelitian ini bertujuan untuk memetakan literatur keislaman yang tersebar dan diakses kalangan pelajar SMA dan mahasiswa di Indonesia.                                                                                '),
(3, 'bg3.jpg', 'Diseminasi Jakarta  2018                         ', 's', 'Narasumber dan Pembahas Diseminasi di Jakarta                                        ');

-- --------------------------------------------------------

--
-- Table structure for table `Slider_project`
--

CREATE TABLE `Slider_project` (
  `id_slider_project` int(5) NOT NULL,
  `Judul` varchar(50) NOT NULL,
  `Gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Slider_project`
--

INSERT INTO `Slider_project` (`id_slider_project`, `Judul`, `Gambar`) VALUES
(1, 'WORKSHOP', 'ab2w.jpg'),
(2, 'FOCUS GROUP DISCUSSION', 'ab3.jpg'),
(3, 'MEDIA CONVEY INDONESIA', 'ab4.jpg'),
(4, 'DISEMINASI JAKARTA', 'ab6.jpg'),
(5, 'DISEMINASI YOGYAKARTA', 'ab5.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `About`
--
ALTER TABLE `About`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `Admin`
--
ALTER TABLE `Admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `Blog`
--
ALTER TABLE `Blog`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `Contact`
--
ALTER TABLE `Contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `Log`
--
ALTER TABLE `Log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `Portofolio`
--
ALTER TABLE `Portofolio`
  ADD PRIMARY KEY (`id_portofolio`);

--
-- Indexes for table `Project`
--
ALTER TABLE `Project`
  ADD PRIMARY KEY (`id_project`);

--
-- Indexes for table `Project_post`
--
ALTER TABLE `Project_post`
  ADD PRIMARY KEY (`id_project_post`);

--
-- Indexes for table `Slider`
--
ALTER TABLE `Slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `Slider_project`
--
ALTER TABLE `Slider_project`
  ADD PRIMARY KEY (`id_slider_project`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `About`
--
ALTER TABLE `About`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `Admin`
--
ALTER TABLE `Admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Blog`
--
ALTER TABLE `Blog`
  MODIFY `id_blog` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `Contact`
--
ALTER TABLE `Contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Log`
--
ALTER TABLE `Log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Portofolio`
--
ALTER TABLE `Portofolio`
  MODIFY `id_portofolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Project`
--
ALTER TABLE `Project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Project_post`
--
ALTER TABLE `Project_post`
  MODIFY `id_project_post` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Slider`
--
ALTER TABLE `Slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Slider_project`
--
ALTER TABLE `Slider_project`
  MODIFY `id_slider_project` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
